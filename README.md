# BERT LID

This repo uses BERT finetuning to classify sentences based on language.

## Content of the repo

* `install_bert.sh`: a script to install the latest version of BERT as a module (`import bert`)
* `src/predictor.py`: wrapper class to use an exported finetuned model
* `src/cherry.py`: little cherrypy server with two endpoints (json, text) for predicting sentences languages
* `src/models`: directory with the finetuned BERT models (both model and code used to train it)

## Available models

### 2019-07-01

**Data**

Langid for North Germanic languages, trained on wikipedia articles (the 30K links) from the Leipzig Corpora collection (the Swiss German data were taken directly from the raw dumps):

* Dutch: *nld_wikipedia_2016*
* Danish:  *dan_wikipedia_2016*
* Estonian: *est-ee_web_2015*
* Slovenian: *slv_wikipedia_2016*
* Swedish: *swe_wikipedia_2016* 
* Luxemburgish: *ltz_wikipedia_2016*
* Finnish: *fin_wikipedia_2016*
* Swiss German: *all-latin-90* (custom)

Each datasource was split evenly between train, dev and test datasets. 
The sentences were normalized using [`norm_punc.py`](https://github.com/derlin/swisstext/blob/3b0f2a6c47050e2a6e5cca5c5ca03b1d84d1d3c7/extra/norm_punc.py)).

The labels are `nld`, `als`, `ltz`, `est`, `slv`, `dan`, `swe` and `fin`.

**Finetuning**

See the notebooks included in the folder. 
Basically, BERT `multi_cased_L-12_H-768_A-12` finetuned with 5K training sentences on 3 epochs (2 minutes). 
Some changes to BERT code were necessary to make it work, e.g. using a regular `Estimator` instead of a `TPUEstimator`, which yielded poor results.


## Usage

Requirements: 

1. A working Python 3 tensorflow (< 2.0) virtualenv
2. BERT (see `install_bert.sh` or [this gist](https://gist.github.com/derlin/2196c17e72a344e45d6f8676d0be53db));
3. packages `cherrypy` and `numpy`

Using the predictor on a file:

```bash
# sentences.txt is a file with one sentence per line
# predictions.csv is the output, with format [class] [sentence] 
python src/predictor.py -i sentences.txt -o predictions.csv
```

Using the cherrypy server:

```bash
# launch the server
python src/cherry.py --prod

# predict sentences using the default (JSON) endpoint
curl -X POST -H 'Content-Type:application/json' \
    -d '{"sentences": ["Das isch sone seich.", "weiss öpper"]}' http://127.0.0.1:8080

# predict sentences using the text endpoint
curl -X POST -H 'Content-Type:text/plain' \
    --data-raw $'Das isch sone seich.\nweiss öpper' http://127.0.0.1:8080/text
```