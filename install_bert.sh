mkdir bert-github && cd bert-github

# clone bert
git clone https://github.com/google-research/bert
# get gist files
wget \
https://gist.githubusercontent.com/derlin/2196c17e72a344e45d6f8676d0be53db/raw/fix_imports.py
wget \
https://gist.githubusercontent.com/derlin/2196c17e72a344e45d6f8676d0be53db/raw/setup.py
# fix imports
python fix_imports.py
# install package
python setup.py install