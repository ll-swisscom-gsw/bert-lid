import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

import numpy as np
import os

DEFAULT_MODEL_DIR = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'models/2019-07-01/exported')

class Predictor():

    def __init__(self, model_dir=DEFAULT_MODEL_DIR, use_gpu_1=False):
        """
        :param model_dir: where the exported model is stored
        """
        self.model_dir = model_dir
        self.config = tf.ConfigProto()
        # Force the use of GPU 1 (usually underused)
        if use_gpu_1:
            import os
            os.environ['CUDA_VISIBLE_DEVICES'] = '0,1'
            config = tf.ConfigProto(
                # force the use of GPU 1
                gpu_options=tf.GPUOptions(allow_growth=True, visible_device_list='1')
            )
        else:
            config = tf.ConfigProto()

        self._read_meta()
        self._load_tokenizer()
        self._load_model(config)

    def _read_meta(self):
        # read the metadata
        import json
        with open(f'{self.model_dir}/meta.json') as f:
            meta = json.load(f)

        self.LABELS_IDX = meta['labels_idx']
        self.LABELS_STR = meta['labels_str']

        self.MAX_SEQ_LENGTH = meta['max_seq_length']
        self.BERT_BASE = meta['bert_base']


    def _load_tokenizer(self):
        # load or create the tokenizer
        if os.path.isfile(f'{self.model_dir}/tokenizer.pickle'):
            import pickle
            with open(f'{self.model_dir}/tokenizer.pickle', 'rb') as f:
                self.tokenizer = pickle.load(f)
        else:
            from bert import tokenization
            self.tokenizer = tokenization.FullTokenizer(
                vocab_file=f'{self.model_dir}/assets/vocab.txt',
                do_lower_case='cased' not in BERT_BASE)


    def _load_model(self, config):
        # load the saved model
        from tensorflow.contrib import predictor
        self._predict_fn = predictor.from_saved_model(self.model_dir, config=config)


    # Data preprocessing functions
    def _preproc_single_sentence(self, s):
        # see https://github.com/google-research/bert/blob/master/run_classifier.py
        segment_ids = [0] * self.MAX_SEQ_LENGTH

        tokens = self.tokenizer.tokenize(s)[:self.MAX_SEQ_LENGTH-2]
        input_ids = self.tokenizer.convert_tokens_to_ids(['[CLS]'] + tokens + ['[SEP]'])

        input_size = len(input_ids)
        padding_size = (self.MAX_SEQ_LENGTH-input_size)

        input_mask = ([1] * input_size) + ([0] * padding_size)
        input_ids += [0] * padding_size

        assert all(len(x) == self.MAX_SEQ_LENGTH for x in [input_ids, segment_ids, input_mask])
        return input_ids, segment_ids, input_mask, 0


    def create_features(self, sentences):
        input_ids, segment_ids, input_mask, labels = zip(*map(self._preproc_single_sentence, sentences))
        return dict(
            input_ids=input_ids,
            segment_ids=segment_ids,
            input_mask=input_mask,
            label_ids=labels
        )


    def predict_proba(self, sentences):
        chunk_size = 100
        chuncks = (sentences[i:i + chunk_size] for i in range(0, len(sentences), chunk_size))
        for chunk in chuncks:
            features = self.create_features(chunk)
            outputs = self._predict_fn(features)
            for s, l, proba in zip(chunk, outputs['prediction'], outputs['probabilities']):
                yield [self.LABELS_STR[l], s, int(l), proba.tolist()]


    def predict(self, sentences):
        for r in self.predict_proba(sentences):
            yield r[:2] # keep only label and sentence



if __name__ == '__main__':
    import argparse, sys, csv
    parser = argparse.ArgumentParser()
    parser.add_argument('--model-dir', default=DEFAULT_MODEL_DIR)
    parser.add_argument('-i', '--file', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('-o', '--out', type=argparse.FileType('w'), default=sys.stdout)
    parser.add_argument('-p', '--proba', action='store_true', help='also print proba')
    args = parser.parse_args()

    lines = [l.strip() for l in args.file if len(l) > 0 and not l.isspace()]
    writer = csv.writer(args.out, delimiter='\t')
    predictor = Predictor(args.model_dir)

    if args.proba:
        writer.writerow(['label','text','label_id'] + predictor.LABELS_STR)
        for row in predictor.predict_proba(lines):
            writer.writerow(row[:-1] + row[-1])
    else:
        writer.writerow(['label','text'])
        for row in predictor.predict(lines):
            writer.writerow(row)
