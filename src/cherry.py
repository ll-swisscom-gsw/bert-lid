from predictor import Predictor, DEFAULT_MODEL_DIR

# ========= server

import cherrypy
import json
import random
import csv
from io import StringIO

class DummyPredictor():
    LABELS_IDX = [0, 1, 2]
    LABELS_STR = ['a', 'b', 'c']

    def predict(self, ss):
        for s in ss:
            yield [random.choice(self.LABELS_STR), s]

    def predict_proba(self, ss):
        for s in ss:
            yield [random.choice(self.LABELS_STR), s, random.choice(self.LABELS_IDX), 
                [random.random()] * len(self.LABELS_STR)]


class WebService(object):

    def __init__(self, predictor):
        self.predictor = predictor

    @cherrypy.expose()
    @cherrypy.tools.allow(methods=['POST'])
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def index(self, return_proba='false'):
        return_proba = self._parse_bool_queryparam(return_proba)
        try:
            lines = cherrypy.request.json['sentences']
            if return_proba:
                keys = ['label','text','label_id', 'proba']
                out = self.predictor.predict_proba(lines)
            else:
                keys = ['label','text']
                out = self.predictor.predict(lines)
            return [{k:v for k,v in zip(keys,r)} for r in out]
        
        except Exception as e:
            raise cherrypy.HTTPError(500, str(e))


    @cherrypy.expose()
    @cherrypy.tools.allow(methods=['POST'])
    @cherrypy.tools.accept(media='text/plain')
    def text(self, return_proba='false'):
        return_proba = self._parse_bool_queryparam(return_proba)
        try:
            body = cherrypy.request.body.read().decode('utf-8').strip()
            if len(body) == 0:
                raise cherrypy.HTTPError(400, message='Body is empty.')
            lines = body.split('\n')

            fileobj = StringIO()
            writer = csv.writer(fileobj)

            if return_proba:
                writer.writerow(['label','text','label_id'] + predictor.LABELS_STR)
                for r in self.predictor.predict_proba(lines):
                    writer.writerow(r[:-1] + r[-1])
                return fileobj.getvalue()
            else:
                writer.writerow(['label','text'])
                for r in self.predictor.predict(lines):
                    writer.writerow(r)
            
            return fileobj.getvalue()
        except Exception as e:
            raise cherrypy.HTTPError(500, message=str(e))


    def _parse_bool_queryparam(self, val):
        val = val.lower()
        if val in ['true', 't', '1', 'yes', '']: 
            return True
        elif val in ['false', 'f', '0', 'no']: 
            return False
        else: 
            raise cherrypy.HTTPError(400, f'wrong boolean value {val}')


def jsonify_error(status, message, traceback, version):
    """ Ensure we return JSON errors """
    cherrypy.response.headers['Content-Type'] = 'application/json'
    try:
        code = int(status.split()[0])
    except:
        code = 500
    return json.dumps({'status_code': code, 'status': status, 'message': message})


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--model-dir', default=DEFAULT_MODEL_DIR)
    parser.add_argument('--port', type=int, default=8080)
    parser.add_argument('--host', type=str, default='127.0.0.1')
    parser.add_argument('--prod', action='store_true', default=False)
    args = parser.parse_args()

    cherrypy.config.update({
        'server.socket_host': args.host,
        'server.socket_port': args.port,
        'error_page.default': jsonify_error,
    })

    if args.prod:
         cherrypy.config.update({
            'global': {'environment' : 'production'}
        })

    predictor = Predictor(args.model_dir)
    #predictor = DummyPredictor()
    cherrypy.quickstart(WebService(predictor))
