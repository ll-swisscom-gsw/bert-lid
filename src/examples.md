using the text endpoint:

```
curl -X POST -H 'Content-Type:text/plain' \
    --data-raw $'Sentence 1\nSentence 2' http://127.0.0.1:8080/text
```

using a text file:

```
curl -X POST -H 'Content-Type:text/plain' \
    --data-binary "@/tmp/swisstext-j-run-2-1k.txt"  http://127.0.0.1:8080/text | tee results.txt
```

using the json endpoint:

```
curl -X POST -H 'Content-Type:application/json' \
    -d '{"sentences": ["Das isch sone ééseich."]}' http://127.0.0.1:8080
```
